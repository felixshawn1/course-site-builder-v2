//package csb.gui;
//
//import csb.CSB_PropertyType;
//import csb.data.Course;
//import csb.data.ScheduleItem;
//import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
//import static csb.gui.CSB_GUI.CLASS_PROMPT_LABEL;
//import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
//import java.time.LocalDate;
//import javafx.event.ActionEvent;
//import javafx.event.EventHandler;
//import javafx.geometry.Insets;
//import javafx.scene.Scene;
//import javafx.scene.control.Button;
//import javafx.scene.control.DatePicker;
//import javafx.scene.control.Label;
//import javafx.scene.control.TextField;
//import javafx.scene.layout.GridPane;
//import javafx.stage.Modality;
//import javafx.stage.Stage;
//import properties_manager.PropertiesManager;
//
///**
// *
// * @author McKillaGorilla
// */
//public class ProgressBarDialog  extends Stage {
//    // THIS IS THE OBJECT DATA BEHIND THIS UI
//    ScheduleItem scheduleItem;
//    
//    // GUI CONTROLS FOR OUR DIALOG
//    GridPane gridPane;
//    Scene dialogScene;
//    Label headingLabel;
//    
//    
//    // CONSTANTS FOR OUR UI
//    public static final String PLEASE_WAIT = "Please Wait...";
//    public static final String BAR_HEADING = "Exporting Index: ";
//          
//
//    /**
//     * Initializes this dialog so that it can be used for either adding
//     * new schedule items or editing existing ones.
//     * 
//     * @param primaryStage The owner of this modal dialog.
//     */
//    public ProgressBarDialog(Stage primaryStage, Course course,  MessageDialog messageDialog) {       
//        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
//        // FOR IT WHEN IT IS DISPLAYED
//        initModality(Modality.WINDOW_MODAL);
//        initOwner(primaryStage);
//        
//        // FIRST OUR CONTAINER
//        gridPane = new GridPane();
//        gridPane.setPadding(new Insets(10, 20, 20, 20));
//        gridPane.setHgap(10);
//        gridPane.setVgap(10);
//        
//        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
//        // ON WHETHER WE'RE ADDING OR EDITING
//        headingLabel = new Label(BAR_HEADING);
//        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
//    
//        
//        // NOW LET'S ARRANGE THEM ALL AT ONCE
//        gridPane.add(headingLabel, 0, 0, 2, 1);
//        
//        // AND PUT THE GRID PANE IN THE WINDOW
//        dialogScene = new Scene(gridPane);
//        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
//        this.setScene(dialogScene);
//    }
//    
//   
//    public ScheduleItem getScheduleItem() { 
//        return scheduleItem;
//    }
//    
//    public double showProgressBarDialog(){
//        setTitle(PLEASE_WAIT);
//        
////        
//    }
//    /**
//     * This method loads a custom message into the label and
//     * then pops open the dialog.
//     * 
//     * @param message Message to appear inside the dialog.
//     */
//    public ScheduleItem showAddScheduleItemDialog(LocalDate initDate) {
//        // SET THE DIALOG TITLE
//        setTitle(ADD_SCHEDULE_ITEM_TITLE);
//        
//        // RESET THE SCHEDULE ITEM OBJECT WITH DEFAULT VALUES
//        scheduleItem = new ScheduleItem();
//        
//        // LOAD THE UI STUFF
//        descriptionTextField.setText(scheduleItem.getDescription());
//        datePicker.setValue(initDate);
//        urlTextField.setText(scheduleItem.getLink());
//        
//        // AND OPEN IT UP
//        this.showAndWait();
//        
//        return scheduleItem;
//    }
//    
//    public void loadGUIData() {
//        // LOAD THE UI STUFF
//       // descriptionTextField.setText(scheduleItem.getDescription());
//        //datePicker.setValue(scheduleItem.getDate());
//       // urlTextField.setText(scheduleItem.getLink());       
//    }
//     
//}