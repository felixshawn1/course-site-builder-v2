package csb.controller;

import static csb.CSB_PropertyType.REMOVE_ITEM_MESSAGE;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.Lecture;
import csb.gui.CSB_GUI;
import csb.gui.MessageDialog;
import csb.gui.LectureItemDialog;
import csb.gui.YesNoCancelDialog;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author McKillaGorilla
 */
public class LectureEditController {
    LectureItemDialog lid;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    public LectureEditController(Stage initPrimaryStage, Course course, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        lid = new LectureItemDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    // THESE ARE FOR SCHEDULE ITEMS
    
    public void handleAddLectureRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        lid.showAddLectureItemDialog();
        
        // DID THE USER CONFIRM?
        if (lid.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            Lecture li = lid.getLectureItem();
            
            // AND ADD IT AS A ROW TO THE TABLE
            course.addLecture(li);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleEditLectureRequest(CSB_GUI gui, Lecture itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        lid.showEditLectureItemDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (lid.wasCompleteSelected()) {
            // UPDATE THE LECTURE ITEM
            Lecture li = lid.getLectureItem();
            itemToEdit.setTopic(li.getTopic());
            itemToEdit.setSessions(li.getSessions());
            
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    
    public void handleRemoveLectureRequest(CSB_GUI gui, Lecture itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeLecture(itemToRemove);
        }
    }
    
    
    //HANDLES THE MOVE UP REQUEST BUTTON
    public void handleMoveUpLectureItemRequest(CSB_GUI gui, ObservableList<Lecture> lectureList, Lecture itemToMove){
        boolean found = false;
        int i =0;
        while(found == false){
            if(lectureList.get(i) == itemToMove){
                found = true;
            }
            else{
                i++;
            }
        }
        //if its not the first item in the list
        if(i!=0){
        Lecture temp = lectureList.get(i-1);
        lectureList.remove(lectureList.get(i));
        lectureList.add(i-1, itemToMove);
        lectureList.add(i, temp);
        lectureList.remove(lectureList.get(i+1));
        }
        else{ 
            //do nothing
        }
    }
    
    //HANDLES THE MOVE DOWN REQUEST BUTTON
    public void handleMoveDownLectureItemRequest(CSB_GUI gui, ObservableList<Lecture> lectureList, Lecture itemToMove){
        boolean found = false;
        int i =0;
        while(found == false){
            if(lectureList.get(i) == itemToMove){
                found = true;
            }
            else{
                i++;
            }
        }
        //if its not the last item in the list
        if(i+1 != lectureList.size()){
        Lecture temp = lectureList.get(i+1);
        lectureList.remove(lectureList.get(i));
        lectureList.add(i+1, itemToMove);
        lectureList.add(i, temp);
        lectureList.remove(lectureList.get(i));
        }
        else{ 
            //do nothing
        }
        
    }
}